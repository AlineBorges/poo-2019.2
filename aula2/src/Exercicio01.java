package aula2;

// Exerc�cio 1

import java.util.*;
import java.text.DecimalFormat;

public class Exercicio01
{
    static final double PI = 3.14159265359;
   
    public static void main(String[] args)
    {
       //Declara��o e Inicializa��o de vari�veis:
        double raio, area;
       
        raio=0;
        area=0;
       
        Scanner tecla = new Scanner(System.in);

       //Entrada de dados:
       System.out.println("Informe o raio do circulo:");
       raio = tecla.nextDouble();
       
       //Processamento:
       area = PI * Math.pow(raio,2);
       
       //Sa�da da informa��o:
       DecimalFormat formatador = new DecimalFormat("0.00");
       System.out.println("A area do circulo: " + formatador.format(area));
       
    }
}
